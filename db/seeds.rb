# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Lama.create(name: "FX", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50", photo: "images/lama.jpg"  )
Lama.create(name: "Yan", category: "fighter", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Clément", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Benjamin", category: "party", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Gabriel", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Dorian", category: "party", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Jason", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Henry", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )
Lama.create(name: "Sylvain", category: "racing", address: "2 rue des saules", zipcode: "59200", city: "Lille", description: "Très sage", price: "50"  )



User.create(first_name: "Sylvain", last_name: "Delva", birthday: "26/11/1997", email: "sylvaindelva@hotmail.fr", password: "12345678")
